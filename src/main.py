import os
import io
import numpy as np 

game_root =  os.path.join(
    os.path.dirname(
        os.path.dirname(
            os.path.dirname(
                os.path.abspath(__file__)
            )
        )
    ),
    "edvin-game"
)

pipetorl = os.path.join(
   game_root,
    "pipetorl"
)

pipetogame = pipe_path = os.path.join(
    game_root,
    "pipetogame"
)

print("Importing env class") 


print("Creating env")
from tensorforce import Environment

class CustomEnvironment(Environment):

    NUM_STATES = 7
    NUM_ACTIONS = 3
    def __init__(self):
        super().__init__()

    def states(self):
        s = dict(type='float', shape=(CustomEnvironment.NUM_STATES,))
        print("States: ", s)
        return s

    def actions(self):
        return dict(type='int', num_values=CustomEnvironment.NUM_ACTIONS)

    # Optional, should only be defined if environment has a natural maximum
    # episode length
    def max_episode_timesteps(self):
        return super().max_episode_timesteps()

    # Optional
    def close(self):
        super().close()

    def reset(self):
        print("Resetting game, make sure game is running!")
        with open(pipetogame, "w") as gm_pipe:
            gm_pipe.writelines(["reset"])
            gm_pipe.close()
        state = None
        with open(pipetorl) as rl_pipe:
            state = np.array([float(x) for x in rl_pipe.readline().split()])
        print("State after reset:", state)
        return state

    def execute(self, actions):
        assert 0 <= actions.item() < CustomEnvironment.NUM_ACTIONS
        action = actions.item()
        print("Action: ", actions)
        if action == 0:
            actions = "1 0 0"
        elif action == 1:
            actions = "0 1 0"
        elif action == 2:
            actions = "0 0 0"
        with open(pipetogame, "w") as gm_pipe:
            gm_pipe.writelines([actions])
            gm_pipe.close()
        print("Action written:", actions)
        next_state = None
        with open(pipetorl) as rl_pipe:
            next_state = np.array([float(x) for x in rl_pipe.readline().split()])
        print("Obtained next state: ", next_state)
        with open(pipetogame, "w") as gm_pipe:
            gm_pipe.writelines(["return-reward"])
            gm_pipe.close()
        terminal = None
        reward = None
        with open(pipetorl) as rl_pipe:
            resp = rl_pipe.readline().split()
            reward = float(resp[0])
            terminal = True if int(resp[1]) == 1 else False
        print("New state: ", next_state)
        print("Reward: ", reward)

        return next_state, terminal, reward

environment = Environment.create(
    environment=CustomEnvironment, max_episode_timesteps=60000
)
print("Env created")

from tensorforce.agents import Agent

print("Creating agent")
agent = Agent.create(
    agent='tensorforce', environment=environment, update=64,
    objective='policy_gradient', reward_estimation=dict(horizon=20),
    # exploration=0.1
    exploration=dict(
        type='decaying', unit='timesteps', decay='exponential',
        initial_value=0.5, decay_steps=10, decay_rate=0.5
    )
)
print("Agent created")

from tensorforce.execution import Runner
runner = Runner(
    agent=agent,
    environment=environment
)

runner.run(num_episodes=200)

runner.run(num_episodes=100, evaluation=True)

runner.close()

# while True:
#     with open(pipetorl) as rl_pipe:
#         print(rl_pipe.readline())
#     with open(pipetogame, "w") as gm_pipe:
#         gm_pipe.write("0 0 1")
#         gm_pipe.close()